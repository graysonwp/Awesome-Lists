# Lowcode List | 低代码构建

# Overview

# Resource 

- [awesome-lowcode #Collection#](https://github.com/taowen/awesome-lowcode): 国内低代码平台从业者交流

## Case Study

- [2019-云凤蝶可视化搭建的推导与实现](https://zhuanlan.zhihu.com/p/90746742): 本文将以概览性的视角来介绍云凤蝶在 低代码+可视化搭建这条路上遇到的问题与解决方案。

## Component Specification

- [2019-WeComponents #Project#](https://github.com/Tencent/WeComponents): WeComponents 是一个基于通用组件语言规范 (CLS) 实现的 Vue.js 声明式组件库，写完 JSON 就做好了页面，让开发变得简单。

# 可视化搭建

- [2020-「可视化搭建系统」——从设计到架构，探索前端领域技术和业务价值](https://zhuanlan.zhihu.com/p/164558106): 前端的边界在哪里，对于业务的价值又在哪里，我们不妨静下来，一起从知乎会员事业部前端实现「可视化搭建系统」的过去和现在，以及未来规划去思考。

# 规则引擎

- [2017-从0到1：构建强大且易用的规则引擎](https://cubox.pro/c/ho3g1x): 首先回顾几个美团点评的业务场景，通过这些场景大家能更好地理解什么是规则，规则的边界是什么。在每个场景后面都介绍了业务系统现在使用的解决方案以及主要的优缺点。 
   
  
 