# Datawarehouse List

- [2019-比起“数据中台”，数据湖更应该被重视](https://cubox.pro/c/gyR0No): 本文将会以“数据湖”为中心，展开讨论数据仓库、数据湖和数据中台这几个概念之间的藕断丝连。

- [2020-辨析数仓、大数据、数据中台的实质    ](https://mp.weixin.qq.com/s/TzcTVUhOq9jhVa1P91RvTw): 关于数仓、数据集市、数据湖、大数据平台以及数据中台，看到一篇从事21年的大佬写的文章分享，将这几者的本质和区别，结合自身经验，讲得偏僻入理，于是转来分享，希望对大家都有所感悟！

# Internals

- [2019-How We Built a Vectorized SQL Engine](https://www.cockroachlabs.com/blog/how-we-built-a-vectorized-sql-engine/#): In this blog post, we use example code to discuss how we built the new engine and why it results in up to a 4x speed improvement on an industry-standard benchmark.
